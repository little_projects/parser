from dotenv import dotenv_values
import logging


config = dotenv_values('.env')

conn_str = config.get('conn_str')
base = config.get('base')
deep = int(config.get('deep'))

# Настройки логера
log = logging.getLogger('parser')
log.setLevel(logging.DEBUG)
fh = logging.FileHandler("parser.log")
formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s %(module)s.%(funcName)s')
fh.setFormatter(formatter)
log.addHandler(fh)