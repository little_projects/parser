from .src.parser import get_and_save_pages
from .config import deep, log


def parse():
    url = 'https://habr.com/ru/articles'
    page_param = 'page'
    page_deep = deep
    log.info("Начали загрузку")
    get_and_save_pages(url, page_param, page_deep, 5)


if __name__ == '__main__':
    parse()
