from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import requests
import json
from .db import get_req_last_record, add_new_record, execute_req
from time import sleep
from sqlalchemy.exc import NoResultFound
from ..config import log


def get_html(link):
    headers = {'User-Agent': UserAgent().random}
    response = requests.get(link, headers=headers)
    rec = dict()
    rec['url'] = response.request.url
    rec['html'] = response.text
    rec['headers'] = dict(response.headers)
    return rec, headers


def get_strap_links(strap_url, page_param, page_deep):
    try:
        last_rec = execute_req(get_req_last_record()).one()[0]
        log.debug('Получена последняя запись из базы')
    except NoResultFound:
        last_rec = ''
        log.warning('В базе нет записей')
    except:
        log.error('База недоступна')
        quit()

    links = []

    for i in range(1, page_deep+1):
        url = f'{strap_url}/{page_param}{i}'
        response = BeautifulSoup(get_html(url)[0]['html'], 'html.parser')
        response = response.find_all('h2', {'class': 'tm-title tm-title_h2'})
        sleep(0.3)
        for h2 in response:
            href = 'https://habr.com' + h2.a.get('href')
            links.append(href)

        if last_rec in links:
            index = links.index(last_rec)
            links = links[:index]
            break
    log.debug(f'Ссылок получено {len(links)}')
    return links[::-1]


def get_and_save_pages(strap_url, page_param, page_deep, sleep_time=5):
    links = get_strap_links(strap_url, page_param, page_deep)
    try:
        for link in links:
            rec = get_html(link)
            slave_rec = json.dumps(rec[1], ensure_ascii=False)
            rec = json.dumps(rec[0], ensure_ascii=False)
            add_new_record(rec, slave_rec)
            sleep(sleep_time)
        log.info('Все записи удачно загружены в базу')
    except:
        log.error('База недоступна')
