from ..config import conn_str, base
from sqlalchemy import create_engine

BASE = base
CONN_STR = conn_str


def execute_req(req, *args):
    engine = create_engine(CONN_STR)
    with engine.connect() as connection:
        cursor = connection.execute(req, args)
    return cursor


def get_req_last_record(source='html', detail='habr'):
    req = """
        select
            main_data #> '{url}' ->> 0 as last_url"""

    req += f"""
        from dwh_lake.{BASE}.requests_statistic
        where "source" = '{source}'
              and rec_id = (select max(rec_id) from dwh_lake.{BASE}.requests_statistic where "source" = '{source}')
    """
    return req


def add_new_record(*params):
    req = f"""insert into {BASE}.requests_statistic (rec_time, rec_type, main_data, second_data, "source", source_detail) """ \
           """values (now(), 2, %s, %s, 'html', 'habr')"""
    execute_req(req, *params)
